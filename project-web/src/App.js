import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import Tugas2 from './sanber/Tugas2/index';
import Routes from './sanber/Routes';
import Islogin from './sanber/Islogin'

function App() {
  return (
    <Router>
      <div>
        <Islogin />
      </div>
    </Router>
  );
}

export default App;
