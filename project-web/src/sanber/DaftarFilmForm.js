import React, { useContext, useState, useEffect } from "react"
import { DaftarFilmContext } from "./DaftarFilmContext"
import axios from "axios"

const DaftarFilmForm = () => {
    // const [input, setInput] = useState({
    //     title: "",
    //     description: "",
    //     year: 0,
    //     duration: 0,
    //     genre: "",
    //     rating: 0
    // })
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [year, setYear] = useState(0);
    const [duration, setDuration] = useState(0);
    const [genre, setGenre] = useState("");
    const [rating, setRating] = useState(0);

    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")
    const [daftarFilm, setDaftarFilm] = useContext(DaftarFilmContext);



    const handleChange1 = (event) => {
        setTitle(event.target.value)
    }
    const handleChange2 = (event) => {
        setDescription(event.target.value)
    }
    const handleChange3 = (event) => {
        setYear(event.target.value)
    }
    const handleChange4 = (event) => {
        setDuration(event.target.value)
    }
    const handleChange5 = (event) => {
        setGenre(event.target.value)
    }
    const handleChange6 = (event) => {
        setRating(event.target.value)
    }



    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        // let title = title
        // let description = description
        // let year = year
        // let duration = duration
        // let genre = genre
        // let rating = rating
        if (rating > 10 || rating < 1) {
            window.location = "./form"
        } else {
            if (statusForm === "create") {
                axios.post(`http://backendexample.sanbercloud.com/api/movies`, {
                    title: title, description: description, year: year,
                    duration: duration, genre: genre, rating: rating
                })
                    .then(res => {
                        setDaftarFilm([
                            ...daftarFilm,
                            {
                                id: res.data.id,
                                title: title, description: description, year: year,
                                duration: duration, genre: genre, rating: rating
                            }])
                    })

            } else if (statusForm === "edit") {
                axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {
                    title: title, description: description, year: year,
                    duration: duration, genre: genre, rating: rating
                })
                    .then(() => {
                        let dataFilm = daftarFilm.find(el => el.id === selectedId)
                        dataFilm.title = title
                        dataFilm.description = description
                        dataFilm.year = year
                        dataFilm.duration = duration
                        dataFilm.genre = genre
                        dataFilm.rating = rating
                        setDaftarFilm([...daftarFilm])
                    })
            }
        }



        // if (title.replace(/\s/g, '') !== "" && description.replace(/\s/g, '') !== "" && year.replace(/\s/g, '') !== "" && duration.replace(/\s/g, '') !== "" && genre.replace(/\s/g, '') !== "" && rating.replace(/\s/g, '') !== "") {


        setStatusForm("create")
        setSelectedId(0)
        setTitle('');
        setDescription('');
        setYear(0);
        setDuration(0);
        setGenre('');
        setRating(0);
        setTimeout(() => {
            window.location = "./"
        }, 500);

    }

    const handleDelete = (event) => {
        let idDaftarFilm = parseInt(event.target.value)

        let newdaftarFilm = daftarFilm.filter(el => el.id != idDaftarFilm)

        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idDaftarFilm}`)
            .then(res => {
                console.log(res)
            })

        setDaftarFilm([...newdaftarFilm])
        setTimeout(() => {
            window.location = "./"
        }, 300);

    }

    const handleEdit = (event) => {
        let idDaftarFilm = parseInt(event.target.value)
        let dataFilm = daftarFilm.find(x => x.id === idDaftarFilm)

        setTitle(dataFilm.title);
        setDescription(dataFilm.description);
        setYear(dataFilm.year);
        setDuration(dataFilm.duration);
        setGenre(dataFilm.genre);
        setRating(dataFilm.rating);

        setSelectedId(idDaftarFilm)
        setStatusForm("edit")
    }




    return (
        <>
            <h1>Form Daftar Film</h1>

            <div style={{ width: "50%", margin: "0 auto", display: "block" }}>
                <div style={{ border: "1px solid #aaa", padding: "20px" }}>
                    <form onSubmit={handleSubmit}>
                        <label style={{ float: "left" }}>
                            Title
                        </label>
                        <input style={{ float: "right" }} type="text" name="title" value={title} onChange={handleChange1} />
                        <br />
                        <br />
                        <label style={{ float: "left" }}>
                            Description:
                        </label>
                        <textarea style={{ float: "right" }} type="text" name="description" value={description} onChange={handleChange2} />
                        <br />
                        <br />
                        <label style={{ float: "left" }}>
                            Year:
                        </label>
                        <input style={{ float: "right" }} type="number" name="year" value={year} onChange={handleChange3} />
                        <br />
                        <br />
                        <label style={{ float: "left" }}>
                            Duration (dalam menit):
                        </label>
                        <input style={{ float: "right" }} type="number" name="duration" value={duration} onChange={handleChange4} />
                        <br />
                        <br />
                        <label style={{ float: "left" }}>
                            Genre:
                        </label>
                        <input style={{ float: "right" }} type="text" name="genre" value={genre} onChange={handleChange5} />
                        <br />
                        <br />
                        <label style={{ float: "left" }}>
                            Rating:
                        </label>
                        <input style={{ float: "right" }} type="number" name="rating" value={rating} onChange={handleChange6} />
                        <br />
                        <br />
                        <div style={{ width: "100%", paddingBottom: "20px" }}>
                            <button style={{ float: "right" }}>submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <h1 style={{ textAlign: "center" }}>Daftar Film</h1>
            <table style={{ width: "80%", margin: "auto", borderCollapse: "collapse" }}>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Duration</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        daftarFilm !== null && daftarFilm.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td>{item.title}</td>
                                    <td>{item.description}</td>
                                    <td>{item.year}</td>
                                    <td>{item.duration}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.rating}</td>
                                    <td>
                                        <button onClick={handleEdit} value={item.id}>Edit</button>
                                         &nbsp;
                                        <button onClick={handleDelete} value={item.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </>
    )
}
export default DaftarFilmForm;