import React from 'react';
import logo from "./public/img/logo.png";
import background from "./public/img/pattern.jpg"
require('./public/css/style.css');

const index = () => {
    return (
        <div className="body" style={{ backgroundImage: `url(${background})`, height: 1000 }}>
            {/* <header className="menu" >
                <img id="logo" src={logo} width="200px" />
                <nav>
                    <ul>
                        <a href="index.html"><li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}>Home</li></a>
                        <a href="about.html"><li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}>About</li></a>
                        <a href="contact.html"><li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}>Contact</li></a>
                    </ul>
                </nav>
            </header> */}
            <section className="container" style={{
                marginLeft: 50,
                marginRight: 50,
                backgroundColor: 'white',
                paddingLeft: 10,
                paddingRight: 10,
                marginTop: 100,
                height: "100%"
            }}>
                <h1>Featured Posts</h1>
                <div id="article-list">
                    <article className="article">
                        <a href=""><h3>Lorem Post 1</h3></a>
                        <p className="section">
                            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                        </p>
                    </article>
                    <article className="article">
                        <a href=""><h3>Lorem Post 2</h3></a>
                        <p className="section">
                            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                        </p>
                    </article>
                    <article className="article">
                        <a href=""><h3>Lorem Post 3</h3></a>
                        <p className="section">
                            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                        </p>
                    </article>
                    <article className="article">
                        <a href=""><h3>Lorem Post 4</h3></a>
                        <p className="section">
                            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                        </p>
                    </article>
                    <article className="article">
                        <a href=""><h3>Lorem Post 5</h3></a>
                        <p className="section">
                            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                        </p>
                    </article>
                </div>
            </section>
            <footer className="footer" style={{ height: 40, justifyContent: 'center' }}>
                <h5>copyright &copy; 2019 by Sanbercode</h5>
            </footer>
        </div >
    );
}

export default index;