import React, { useState, useEffect } from 'react';
import { DaftarFilmProvider } from "./DaftarFilmContext";
import DaftarFilmForm from "./DaftarFilmForm"

const Form = () => {
    return (
        <DaftarFilmProvider>
            <DaftarFilmForm />
        </DaftarFilmProvider>
    )
}

export default Form