import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import Tugas2 from './Tugas2/index';
import { DaftarFilmContext } from "./DaftarFilmContext"
import About from './about';
import logo from "./Tugas2/public/img/logo.png";
import Form from './form';
import Home from './Home';
import Login from './Login';
import Routes from './Routes';
require('./Tugas2/public/css/style.css');


const Routes2 = () => {
    return (
        <div className="body" style={{ height: 600 }}>
            <header className="menu" >
                <img id="logo" src={logo} width="200px" />
                <nav>
                    <ul>
                        <li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}><Link to="/">Home</Link></li>
                        <li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}><Link to="/about">about</Link></li>
                        <li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}><Link to="/login">Login</Link></li>

                    </ul>
                </nav>
            </header>
            {/* <ul style={{ left: 0 }}>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/tugas11">about</Link>
                </li>
                <li>
                    <Link to="/tugas13">tugas13</Link>
                </li>
                <li>
                    <Link to="/tugas14">tugas14</Link>
                </li>
                <li>
                    <Link to="/tugas14">tugas14</Link>
                </li>
                <li>
                    <Link to="/databuah">Databuah</Link>
                </li>

            </ul> */}
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/about">
                    <About />
                </Route>
                <Route exact path="/login">
                    <Login />
                </Route>
                <Route exact path="/is">
                    <Routes />
                </Route>
                {/* <Route exact path="/home">
                    <Home />
                </Route> */}
                {/* <Route path="/tugas13">
                    <Tugas13 />
                </Route>
                <Route exact path="/tugas14">
                    <Tugas14 />
                </Route>
                <Route exact path="/databuah">
                    <Databuah />
                </Route> */}
            </Switch>
        </div>
    )
}
export default Routes2;