import React, { useState, createContext, useEffect } from "react";
import axios from "axios"

export const RoutesContext = createContext();

export const RoutesProvider = props => {

    const [login, setLogin] = useState(0);
    return (
        <RoutesContext.Provider value={[login, setLogin]}>
            {props.children}
        </RoutesContext.Provider>
    )
}