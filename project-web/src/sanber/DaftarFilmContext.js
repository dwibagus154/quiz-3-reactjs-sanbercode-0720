import React, { useState, createContext, useEffect } from "react";
import axios from "axios"

export const DaftarFilmContext = createContext();

export const DaftarFilmProvider = props => {

    const [daftarFilm, setDaftarFilm] = useState(null)
    useEffect(() => {
        if (daftarFilm === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
                .then(res => {
                    setDaftarFilm(res.data.map(el => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating,

                        }
                    }))
                })
        }
    }, [daftarFilm])

    return (
        <DaftarFilmContext.Provider value={[daftarFilm, setDaftarFilm]}>
            {props.children}
        </DaftarFilmContext.Provider>
    )
}