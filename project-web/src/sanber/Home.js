import React, { useState, useEffect } from 'react';
import { DaftarFilmProvider } from "./DaftarFilmContext";
import DaftarFilmList from "./DaftarFilmList"

const Home = () => {
    return (
        <DaftarFilmProvider>
            <DaftarFilmList />
        </DaftarFilmProvider>
    )
}

export default Home



// import background from "./Tugas2/public/img/pattern.jpg"
// import axios from "axios"
// require('./Tugas2/public/css/style.css');

// const Home = () => {

//     const [daftarFilm, setDaftarFilm] = useState(null)
//     useEffect(() => {
//         if (daftarFilm === null) {
//             axios.get(`http://backendexample.sanbercloud.com/api/movies`)
//                 .then(res => {
//                     setDaftarFilm(res.data.map(el => { return { id: el.id, title: el.title, rating: el.rating, duration: el.duration, genre: el.genre, description: el.description } }))
//                 })
//         }
//     }, [daftarFilm])

//     return (
//         <div className="body" style={{ backgroundImage: `url(${background})`, height: "100%", flex: 1 }}>
//             {/* <header className="menu" >
//                 <img id="logo" src={logo} width="200px" />
//                 <nav>
//                     <ul>
//                         <a href="index.html"><li className="li" style={{
//                             display: 'inline-block',
//                             margin: 8
//                         }}>Home</li></a>
//                         <a href="about.html"><li className="li" style={{
//                             display: 'inline-block',
//                             margin: 8
//                         }}>About</li></a>
//                         <a href="contact.html"><li className="li" style={{
//                             display: 'inline-block',
//                             margin: 8
//                         }}>Contact</li></a>
//                     </ul>
//                 </nav>
//             </header> */}
//             <section className="container" style={{
//                 marginLeft: 50,
//                 marginRight: 50,
//                 backgroundColor: 'white',
//                 paddingLeft: 10,
//                 paddingRight: 10,
//                 marginBottom: 100,
//                 marginTop: 100,
//                 height: "100%"
//             }}>
//                 <h1>Daftar Film Film Terbaik</h1>
//                 <div id="article-list">
//                     {
//                         daftarFilm !== null && daftarFilm.map((item, index) => {
//                             return (
//                                 <article key={index} className="article">
//                                     <p style={{ color: "skyblue" }}>{item.title}</p>
//                                     <br />
//                                     <p>Rating: {item.rating}</p>
//                                     <p>Durasi: {item.duration} jam</p>
//                                     <p>Genre: {item.genre}</p>
//                                     <p className="section">
//                                         <strong>deskripsi</strong> {item.description}
//                                     </p>
//                                 </article>
//                             )
//                         })
//                     }
//                 </div>
//             </section>
//             <footer className="footer" style={{ marginTop: 20, height: 40, justifyContent: 'center', bottom: 0, position: "fixed", width: "100%" }}>
//                 <h5>copyright &copy; 2019 by Sanbercode</h5>
//             </footer>
//         </div >
//     );
// }

// export default Home;