import React, { useContext, useEffect, useState } from "react"
import { DaftarFilmContext } from "./DaftarFilmContext"
import background from "./Tugas2/public/img/pattern.jpg"
import axios from "axios"
require('./Tugas2/public/css/style.css');

const DaftarFilmList = () => {
    const [daftarFilm, setDaftarFilm] = useContext(DaftarFilmContext);
    let dataFilm = daftarFilm;
    console.log(dataFilm);
    // dataFilm.sort(function (a, b) {
    //     return a.rating - b.rating;
    // });

    return (
        <div className="body" style={{ backgroundImage: `url(${background})`, height: "100%", flex: 1 }}>
            {/* <header className="menu" >
                <img id="logo" src={logo} width="200px" />
                <nav>
                    <ul>
                        <a href="index.html"><li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}>Home</li></a>
                        <a href="about.html"><li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}>About</li></a>
                        <a href="contact.html"><li className="li" style={{
                            display: 'inline-block',
                            margin: 8
                        }}>Contact</li></a>
                    </ul>
                </nav>
            </header> */}
            <section className="container" style={{
                marginLeft: 50,
                marginRight: 50,
                backgroundColor: 'white',
                paddingLeft: 10,
                paddingRight: 10,
                marginBottom: 100,
                marginTop: 100,
                height: "100%"
            }}>
                <h1>Daftar Film Film Terbaik</h1>
                <div id="article-list">
                    {
                        daftarFilm !== null && daftarFilm.map((item, index) => {
                            return (
                                <article key={index} className="article">
                                    <p style={{ color: "skyblue" }}>{item.title}</p>

                                    <p>Rating: {item.rating}</p>
                                    <p>Durasi: {item.duration} jam</p>
                                    <p>Genre: {item.genre}</p>
                                    <p className="section">
                                        <strong>deskripsi</strong> {item.description}
                                    </p>
                                </article>
                            )
                        })
                    }
                </div>
            </section>
            <footer className="footer" style={{ marginTop: 20, height: 40, justifyContent: 'center', bottom: 0, position: "fixed", width: "100%" }}>
                <h5>copyright &copy; 2019 by Sanbercode</h5>
            </footer>
        </div >
    );
}

export default DaftarFilmList;